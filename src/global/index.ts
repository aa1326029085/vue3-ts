import { App } from 'vue'
import registerProperties from './register-properties'

export default function (app: App) {
  registerProperties(app)
}
