import { ref } from 'vue'
import type PageModal from '@/components/page-modal'

type btnCb = (item?: any) => void

export default function usePageModal(
  newCallback?: btnCb,
  editCallback?: btnCb
) {
  const pageModalRef = ref<InstanceType<typeof PageModal>>()
  //初始化数据1
  const defaultInfo = ref({})
  //新建
  const handleNewData = () => {
    defaultInfo.value = {}
    if (pageModalRef.value) {
      pageModalRef.value.dialogVisible = true
    }
    newCallback && newCallback()
  }
  //编辑
  const handleEditData = (item: any) => {
    defaultInfo.value = { ...item }
    if (pageModalRef.value) {
      pageModalRef.value.dialogVisible = true
    }
    editCallback && editCallback(item)
  }

  return [handleNewData, handleEditData, pageModalRef, defaultInfo]
}
