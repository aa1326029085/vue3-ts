import { ref } from 'vue'

import type PageContent from '@/components/page-content'

const usePageSearch = () => {
  const pageContentRef = ref<InstanceType<typeof PageContent>>()
  const resetBtnClick = () => {
    pageContentRef.value?.getPageData()
  }

  const searchBtnClick = (queryInfo: any) => {
    pageContentRef.value?.getPageData(queryInfo)
  }

  return [pageContentRef, resetBtnClick, searchBtnClick]
}

export default usePageSearch
