import { useStore } from '@/store'

export function usePermission(pageName: string, handleName: string) {
  const store = useStore()
  const permissions = store.state.loginModule.permissions
  const verifyPermission = `system:${pageName}:${handleName}`
  //将一个其他类型转换成boolean类型
  return !!permissions.find((item) => item === verifyPermission)
}
