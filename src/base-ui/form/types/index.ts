type IFormType = 'input' | 'password' | 'select' | 'datepicker'

export interface IFormItem {
  //双向绑定字段
  field: string
  //生成的表单类型 （input或者select等等）
  type: IFormType
  //表单前的展示字段
  label: string
  //规则
  rules?: any[]
  //默认展示字段
  placeholder?: any
  // 针对select
  options?: any[]
  // 针对特殊的属性
  otherOptions?: any
  isHidden?: boolean
}

export interface IForm {
  formItems: IFormItem[]
  labelWidth?: string
  colLayout?: any
  itemStyle?: any
}
