import { App, createVNode } from 'vue'
import * as Icons from '@element-plus/icons-vue'

export default function registerIcons(app: App) {
  // 创建Icon组件
  const Icon = (props: { icon: string }) => {
    const { icon } = props
    return createVNode(Icons[icon as keyof typeof Icons])
  }
  // 注册Icon组件
  // eslint-disable-next-line vue/multi-word-component-names
  app.component('Icon', Icon)
}
