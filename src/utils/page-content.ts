import { App } from 'vue'
//1.引入封装好的全局组件地址
import PageContent from '@/components/page-content'
//2.导出
export default {
  install(app: App) {
    // 此处形参为main.js文件中use()方法自动传进来的Vue实例
    app.component('PageContent', PageContent)
  }
}
