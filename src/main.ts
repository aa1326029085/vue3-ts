import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { createPinia } from 'pinia'

import 'normalize.css'
import './assets/css/index.less'
import 'element-plus/dist/index.css'
import zhCn from 'element-plus/es/locale/lang/zh-cn'
import ElementPlus from 'element-plus'

import registerIcons from './utils/icon'
import { setupStore } from './store'
import Allregister from './global'

import PageContent from '@/utils/page-content'
import PageSearch from '@/utils/page-search'
import PageModal from '@/utils/page-modal'

const pinia = createPinia()

const app = createApp(App)

app.use(store)
app.use(pinia)
app.use(setupStore)
app.use(router)
app.use(Allregister)
app.use(registerIcons)
app.use(ElementPlus, {
  locale: zhCn
})
app.use(PageContent)
app.use(PageSearch)
app.use(PageModal)

app.mount('#app')
