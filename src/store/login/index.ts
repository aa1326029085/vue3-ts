import router from '@/router'

import type { Module } from 'vuex'
import type { ILoginState } from './types'
import type { IRootState } from '../types'
import type { IAccount } from '@/service/login/type'

import {
  accountLoginRequest,
  requestUserInfoById,
  requestUserMenusByRoleId
} from '@/service/login/index'
import LocalCache from '@/utils/cache'
import { mapMenusToRoutes, mapMenusToPermissions } from '@/utils/map-menus'

//Module必须接收两个泛型
const loginModule: Module<ILoginState, IRootState> = {
  namespaced: true,
  state() {
    return {
      token: '',
      userInfo: {},
      userMenus: [],
      permissions: []
    }
  },
  mutations: {
    changeToken(state, payload: string) {
      state.token = payload
    },
    changeUserInfo(state, payload: any) {
      state.userInfo = payload
    },
    changeUserMenus(state, payload: any) {
      state.userMenus = payload
      //注册动态路由
      // userMenus => routes
      const routes = mapMenusToRoutes(payload)

      // 将routes => router.main.children
      routes.forEach((route) => {
        router.addRoute('main', route)
      })

      // 获取用户按钮的权限
      const permissions = mapMenusToPermissions(payload)
      state.permissions = permissions
    }
  },
  actions: {
    //Login数据请求
    async accountLoginAction({ commit, dispatch }, payload: IAccount) {
      //1.登陆逻辑
      const LoginResult = await accountLoginRequest(payload)
      console.log('LoginResult', LoginResult)
      const { id, token } = LoginResult.data
      commit('changeToken', token)
      LocalCache.setCache('token', token)

      // 发送初始化的请求(完整的role/department)
      dispatch('getInitialDataAction', null, { root: true })

      //2.id请求用户信息
      const userInfoResult = await requestUserInfoById(id)
      console.log('userInfoResult', userInfoResult)
      const userInfo = userInfoResult.data
      commit('changeUserInfo', userInfo)
      LocalCache.setCache('userInfo', userInfo)

      //3.请求用户权限的菜单
      const userMenusResult = await requestUserMenusByRoleId(id)
      console.log('userMenusResult', userMenusResult)
      const userMenus = userMenusResult.data
      commit('changeUserMenus', userMenus)
      LocalCache.setCache('userMenus', userMenus)

      //4.跳转到首页
      router.push('/main')
    },
    //缓存数据
    LoadLocalLogin({ commit }) {
      const token = LocalCache.getCache('token')
      if (token) {
        commit('changeToken', token)
      }
      const userInfo = LocalCache.getCache('userInfo')
      if (userInfo) {
        commit('changeUserInfo', userInfo)
      }
      const userMenus = LocalCache.getCache('userMenus')
      if (userMenus) {
        commit('changeUserMenus', userMenus)
      }
    }
  }
}

export default loginModule
