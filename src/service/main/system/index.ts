import CLFRequest from '@/service'
import { IDataType } from '@/service/types'

export function getPageListData(url: string, queryInfo: any) {
  return CLFRequest.post<IDataType>({
    url,
    data: queryInfo,
    showLoading: false
  })
}

// url: /users/id
//删除
export function deletePageData(url: string) {
  return CLFRequest.delete<IDataType>({
    url: url
  })
}
//新增
export function createPageData(url: string, newData: any) {
  return CLFRequest.post<IDataType>({
    url: url,
    data: newData
  })
}
//编辑
export function editPageData(url: string, editData: any) {
  return CLFRequest.patch<IDataType>({
    url: url,
    data: editData
  })
}
