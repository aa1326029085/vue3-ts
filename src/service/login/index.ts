import CLFRequest from '../index'
import type { IAccount, IDataType, ILoginResult } from './type'

enum LoginAPI {
  AccountLogin = '/login',
  LoginUserInfo = '/users/',
  UserMenus = 'role/'
}
//登陆token请求
export function accountLoginRequest(account: IAccount) {
  return CLFRequest.post<IDataType<ILoginResult>>({
    url: LoginAPI.AccountLogin,
    data: account,
    showLoading: false
  })
}
//id请求用户信息
export function requestUserInfoById(id: number) {
  return CLFRequest.get<IDataType>({
    url: LoginAPI.LoginUserInfo + id,
    showLoading: false
  })
}
//role/id请求当前用户可见的菜单
export function requestUserMenusByRoleId(id: number) {
  return CLFRequest.get<IDataType>({
    url: LoginAPI.UserMenus + id + '/menu',
    showLoading: false
  })
}
