import CLFRequest from './request'
import LocalCache from '@/utils/cache'
import { BASE_URL, TIME_OUT } from './request/config'

const request = new CLFRequest({
  baseURL: BASE_URL,
  timeout: TIME_OUT,
  interceptors: {
    requestInterceptor: (config) => {
      const token = LocalCache.getCache('token')
      if (token) {
        config.headers!.Authorization = `Bearer ${token}`
        // if (config.headers) {
        //   config.headers.Authorization = `Bearer ${token}`
        // }
      }
      return config
    }
    // requestInterceptorCatch: (err) => {
    //   console.log('请求失败的拦截')
    //   return err
    // },
    // responseInterceptor: (config) => {
    //   console.log('响应成功的拦截')
    //   return config
    // },
    // responseInterceptorCatch: (err) => {
    //   console.log('响应失败的拦截')
    //   return err
    // }
  }
})
export default request
