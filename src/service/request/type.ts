import type { AxiosRequestConfig, AxiosResponse } from 'axios'

export interface CLFRequestInterceptor<T = AxiosResponse> {
  requestInterceptor?: (config: AxiosRequestConfig) => AxiosRequestConfig
  requestInterceptorCatch?: (err: any) => any
  responseInterceptor?: (config: T) => T
  responseInterceptorCatch?: (err: any) => any
}

export interface CLFRequestConfig<T = AxiosResponse>
  extends AxiosRequestConfig {
  interceptors?: CLFRequestInterceptor<T>
  showLoading?: boolean
}
